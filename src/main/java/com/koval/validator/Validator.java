package com.koval.validator;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.exceptions.ProcessingException;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;
import com.github.fge.jsonschema.report.ProcessingReport;
import java.io.File;
import java.io.IOException;

public class Validator {

    public static boolean validate(final File filePath, final File schemaPath) throws IOException, ProcessingException {
        JsonNode file = JsonLoader.fromFile(filePath);
        JsonNode schema = JsonLoader.fromFile(schemaPath);
        JsonSchemaFactory jsonSchemaFactory = JsonSchemaFactory.byDefault();
        JsonValidator jsonValidator = jsonSchemaFactory.getValidator();
        ProcessingReport processingReport = jsonValidator.validate(schema,file);
        return processingReport.isSuccess();
    }
}