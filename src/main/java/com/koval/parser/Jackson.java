package com.koval.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.koval.model.Gun;
import com.koval.sorter.Sortable;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;

public class Jackson implements Sortable {

    private static Gun[]guns;

    public final Gun[] read() throws IOException {
        final String filePath = "src/main/resources/guns.json";
        ObjectMapper objectMapper = new ObjectMapper();
        guns = objectMapper.readValue(new FileReader(filePath),Gun[].class);
        sort(guns);
        return guns;
    }

    public final void write() throws IOException {
        final String filePath = "jackson.json";
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(new File(filePath),guns);
    }

    @Override
    public void sort(Gun[]guns) {
        Comparator<Gun> gunComparator = new Comparator<Gun>() {
            @Override
            public int compare(Gun o1, Gun o2) {
                return o1.getModelName().compareTo(o2.getModelName());
            }
        };
        Arrays.sort(guns,gunComparator);
    }
}
