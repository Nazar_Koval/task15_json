package com.koval.parser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.koval.model.Gun;
import com.koval.sorter.Sortable;
import java.io.*;
import java.util.Arrays;
import java.util.Comparator;

public class GSON implements Sortable {

    private static Gun[]guns;

    public final Gun[] read() throws FileNotFoundException {
        final String filePath = "src/main/resources/guns.json";
        Gson gson = new Gson();
        guns = gson.fromJson(new FileReader(filePath),Gun[].class);
        sort(guns);
        return guns;
    }

    public final void write() throws IOException {
        final File filePath = new File("gson.json");
        try(Writer writer = new FileWriter(filePath)) {
            Gson gson = new GsonBuilder().create();
            gson.toJson(guns, writer);
        }
    }

    @Override
    public void sort(Gun[]guns) {
        Comparator<Gun>gunComparator = new Comparator<Gun>() {
            @Override
            public int compare(Gun o1, Gun o2) {
                return o1.getTtc().getRange().compareTo(o2.getTtc().getRange());
            }
        };
        Arrays.sort(guns,gunComparator);
    }
}
