package com.koval.sorter;

import com.koval.model.Gun;

@FunctionalInterface
public interface Sortable {
    void sort(Gun[]guns);
}
