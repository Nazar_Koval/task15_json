package com.koval.model;

public class Gun {

    private String modelName;
    private Integer handy;
    private String origin;
    private String material;
    private TTC ttc;

    public Gun(){
        ttc = new TTC();
    }

    public Gun(final String modelName, final Integer handy, final String origin, final String material,
               final Integer range, final Integer scope, final Boolean clip, final Boolean optic){
        this.modelName = modelName;
        this.handy = handy;
        this.origin = origin;
        this.material = material;
        this.ttc = new TTC(range,scope,clip,optic);
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(final String modelName) {
        this.modelName = modelName;
    }

    public Integer getHandy() {
        return handy;
    }

    public void setHandy(final Integer handy) {
        this.handy = handy;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(final String origin) {
        this.origin = origin;
    }

    public TTC getTtc() {
        return ttc;
    }

    public void setTtc(final TTC ttc) {
        this.ttc = ttc;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(final String material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "Gun{" +
                "modelName='" + modelName + '\'' +
                ", handy=" + handy +
                ", origin='" + origin + '\'' +
                ", ttc=" + ttc +
                ", material='" + material + '\'' +
                '}';
    }
}
