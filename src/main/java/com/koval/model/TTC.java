package com.koval.model;

public class TTC {

    private Integer range;
    private Integer scope;
    private Boolean clip;
    private Boolean optic;

    public TTC(){}

    public TTC(final Integer range, final Integer scope, final Boolean clip, final Boolean optic){
        this.range = range;
        this.scope = scope;
        this.clip = clip;
        this.optic = optic;
    }

    public Integer getRange() {
        return range;
    }

    public void setRange(final Integer range) {
        this.range = range;
    }

    public Integer getScope() {
        return scope;
    }

    public void setScope(final Integer scope) {
        this.scope = scope;
    }

    public Boolean isClip() {
        return clip;
    }

    public void setClip(final Boolean clip) {
        this.clip = clip;
    }

    public Boolean isOptic() {
        return optic;
    }

    public void setOptic(final Boolean optic) {
        this.optic = optic;
    }

    @Override
    public String toString() {
        return "TTC{" +
                "range=" + range +
                ", scope=" + scope +
                ", clip=" + clip +
                ", optic=" + optic +
                '}';
    }
}
