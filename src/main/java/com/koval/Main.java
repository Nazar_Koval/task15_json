package com.koval;

import com.koval.model.Gun;
import com.koval.parser.GSON;
import com.koval.parser.Jackson;
import org.apache.logging.log4j.*;
import java.io.IOException;
import java.util.Arrays;

public class Main {

    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) throws IOException{

        logger.trace("~~~~~~~GSON~~~~~~~\n");
        GSON gson = new GSON();
        Gun[] gunsGson = gson.read();
        logger.trace("Read from file:\n");
        logger.trace(Arrays.toString(gunsGson));
        logger.trace("\nWriting sorted by range array to gson.json..\n");
        gson.write();

        logger.trace("\n~~~~~~~Jackson~~~~~~~\n");
        Jackson jackson = new Jackson();
        Gun[] gunsJackson = jackson.read();
        logger.trace("Read from file:\n");
        logger.trace(Arrays.toString(gunsJackson));
        logger.trace("\nWriting sorted by name array to jackson.json..\n");
        jackson.write();

    }

}
